package kr.co.everspin.practice;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@WebListener
public class HttpServletConfiguration implements ServletContextListener {
	
	private static final Logger logger = LoggerFactory.getLogger(HttpServletConfiguration.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		logger.info("Practice App Server contextInitialized");
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		logger.info("Practice App Server contextDestroyed");
	}


}
