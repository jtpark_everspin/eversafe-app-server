package kr.co.everspin.practice.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import kr.co.everspin.practice.dto.LoginDto;
import kr.co.everspin.practice.dto.ResponseDto;

@WebServlet("/login")
public class PracticeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	Gson gson = new Gson();
	
	private static final Logger logger = LoggerFactory.getLogger(PracticeServlet.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			process(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			process(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void process(HttpServletRequest req, HttpServletResponse resp) throws Exception {

		ResponseDto responseDto = new ResponseDto();
		
		try {
			
			logger.info("request URL : {}", req.getRequestURL());
			logger.info("request METHOD : {}", req.getMethod());
			if(req.getQueryString() != null && !req.getQueryString().isEmpty())
				logger.info("request QUERYSTRING : {}", req.getQueryString());
			
			String body = null;
			
			body = getBodyString(req);
				
			
			if(body != null && !body.isEmpty())
				logger.debug("BODY : {}", body);

			LoginDto loginDto = this.gson.fromJson(body, LoginDto.class);
			logger.debug("Login Info : {}", gson.toJson(loginDto));

			if (loginDto == null || loginDto.getId() == null || loginDto.getPassword() == null) {
				loginFailed(responseDto, "Invalid Request.");
				return;
			}
			
			if (auth(loginDto))
				loginSuccess(responseDto);
			else
				loginFailed(responseDto, "ID and password do not match.");

		} catch (Exception e) {
			logger.warn(e.getMessage(), e);
			loginFailed(responseDto, e.getMessage());
		} finally {
			logger.info("Result : {}", gson.toJson(responseDto));
			writeResponse(resp, responseDto);
		}

	}

	private boolean auth(LoginDto loginDto) {
		
		if(!loginDto.getId().equals("everspin")) {
			logger.info("ID Not Matched");
			return false;
		}
		
		if(!loginDto.getPassword().equals("eversafe")) {
			logger.info("password Not Matched");
			return false;
		}
		
		return true;
	}
	
	private void writeResponse(HttpServletResponse resp, ResponseDto responseDto) throws Exception {
		
		String jsonString = this.gson.toJson(responseDto);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		PrintWriter pw = null;
		try {
			pw = resp.getWriter();
			pw.print(jsonString);
			pw.flush();
		} catch (Exception e) {
			throw e;
		} finally {
			if(pw != null) {
				try { pw.close(); } catch (Exception e2) {}
			}
		}
	}

	private void loginSuccess(ResponseDto responseDto) throws IOException {
		responseDto.setResult(1);
		responseDto.setMsg("Login Success.");

	}

	private void loginFailed(ResponseDto responseDto, String msg) throws IOException {
		responseDto.setResult(0);
		
		if(msg == null || msg.isEmpty()) {
			msg = "EMPTY";
		}
		responseDto.setMsg(msg);
	}

	public static String getBodyString(HttpServletRequest request) throws IOException {

		String body = null;
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = null;
		InputStream inputStream = null;

		try {
			inputStream = request.getInputStream();
			if (inputStream != null) {
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				char[] charBuffer = new char[128];
				int bytesRead = -1;
				while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
					stringBuilder.append(charBuffer, 0, bytesRead);
				}
			} else {
				stringBuilder.append("");
			}
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (bufferedReader != null) {
				try { bufferedReader.close(); } catch (IOException ex) { }
			}
			
			if (inputStream != null) {
				try { inputStream.close(); } catch (IOException ex) { }
			}
		}

		body = stringBuilder.toString();
		return body;
	}

}
